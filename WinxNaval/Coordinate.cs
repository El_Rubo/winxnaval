﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinxNaval
{
    class Coordinate
    {
        public int x; public int y;

        public Coordinate()
        {
            x = 0;
            y = 0;
        }
        public Coordinate(int newx, int newy)
        {
            setX(newx); setY(newy);
        }

        int getX()
        {
            return x;
        }
        int getY()
        {
            return y;
        }
        void setX(int newx)
        {
            x = newx;
        }
        void setY(int newy)
        {
            y = newy;
        }


    }
}
