﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinxNaval
{
     class Celula
    {
        public bool temNavio;
        public bool isDead;
        public bool isBurning;
        private Navio barco;

        public bool Discovered { get; internal set; }

        public Navio GetBarco()
        {
            return barco;
        }

        public void SetBarco(Navio value)
        {
            barco = value;
        }

        public Celula()
        {
            temNavio = false;
            isDead = false;
            isBurning = false;
            Discovered = false;
        }

        public Celula(Char c)
        {
            this.fromChar(c);
        }

        public void fromChar(Char c)
        {
            switch (c)
            {
                case 'X':
                    this.temNavio = false; this.isDead = false;
                    this.isBurning = false; this.Discovered = true;
                    break;
                default:
                case '-':
                    this.temNavio = false; this.isDead = false;
                    this.isBurning = false; this.Discovered = false;
                    break;
                case 'F':
                    this.temNavio = true; this.isDead = false;
                    this.isBurning = true; this.Discovered = false;
                    break;
                case 'D':
                    this.temNavio = true; this.isDead = true;
                    this.isBurning = false; this.Discovered = false;
                    break;
                case 'O':
                    this.temNavio = true; this.isDead = false;
                    this.isBurning = false; this.Discovered = false;
                    break;
            }
        }

        public bool isOccupied()
        {
            return temNavio;
        }

        public void setOccupied(bool t) {
            temNavio = t;
        }

        internal Navio Navio
        {
            get => default(Navio);
            set
            {
            }
        }
    }
}
