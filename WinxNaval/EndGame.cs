﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinxNaval
{
    public partial class EndGame : Form
    {
        public EndGame(bool Win)
        {
            InitializeComponent();
            if (Win)
                this.BackgroundImage = new Bitmap(AppDomain.CurrentDomain.BaseDirectory + "YouWin.png");
            else
                this.BackgroundImage = new Bitmap(AppDomain.CurrentDomain.BaseDirectory + "YouLose.png");
        }
    }
}
